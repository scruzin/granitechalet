#!/usr/bin/env python
#
# Name: granitechalet
# URLs: www.granitechalet.com, ranitechalet.appspot.com
# App Engine Owner: <anoble@gmail.com>
#
# Revision History:
#   17 Mar 2021

import datetime

from flask import Flask, render_template

app = Flask(__name__, template_folder='templates', static_url_path='/images', static_folder="images")

@app.route('/')
def root():
  return render_template('index.html')

@app.route('/thingstodo.html')
def thingstodo():
  return render_template('thingstodo.html')

@app.route('/bestof.html')
def bestof():
  return render_template('bestof.html')

@app.route('/history.html')
def history():
  return render_template('history.html')

@app.route('/directions.html')
def directions():
  return render_template('directions.html')

@app.route('/map.html')
def map():
  return render_template('map.html')

if __name__ == '__main__':
    # This is used only when running locally, e.g., python main.py.
    # When deploying to App Engine app.yaml specifies the entry point.
    app.run(host='127.0.0.1', port=8080, debug=True)
